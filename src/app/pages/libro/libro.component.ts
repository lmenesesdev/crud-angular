import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgForm, FormControl, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { map, startWith} from 'rxjs/operators';
import { LibroModel } from '../../models/libro.model';
import { LibrosService } from '../../services/libros.service';
import { CategoriasService } from '../../services/categorias.service';
import { AutoresService } from '../../services/autores.service';
import { CategoriaModel } from 'src/app/models/categoria.model';
import { AutorModel } from 'src/app/models/autor.model';

import Swal from 'sweetalert2';



@Component({
  selector: 'app-libro',
  templateUrl: './libro.component.html',
  styleUrls: ['./libro.component.css']
})
export class LibroComponent implements OnInit {

  libro: LibroModel = new LibroModel();
  categorias: CategoriaModel[] = [];
  autores: AutorModel[] = [];
  formularioError: boolean = false;

  autor = new FormControl();
  categoria =  new FormControl();
  autoresFiltrados: Observable<AutorModel[]>;
  categoriaControl = new FormControl('', [Validators.required]);



  constructor( private librosService: LibrosService,
               private categoriasService: CategoriasService,
               private autoresService: AutoresService,
               private route: ActivatedRoute ) { }

  ngOnInit() {

    const id = this.route.snapshot.paramMap.get('id');

    this.categoriasService.getCategorias().subscribe( resp => {
      console.log(resp);
      this.categorias = resp;
    });

    this.autoresService.getAutores().subscribe( resp => {
      this.autores = resp;  
    });

    this.autoresFiltrados = this.autor.valueChanges
    .pipe(
      startWith(''),
      map(value => this._filter(value))
    );

 
    

    if ( id !== 'nuevo' ) {

      this.librosService.getLibro( id )
        .subscribe( (resp: LibroModel) => {
          console.log(resp);
          this.libro = resp;
          this.categoriaControl.patchValue(resp.categoriaId);
          this.autor.patchValue(new AutorModel(resp.autor));
          this.libro.id = id;
        });

    }

  }

  private _filter(value: any): AutorModel[] {

    const filterValue = value.nombre != undefined ? value.nombre : value.toLowerCase();

    return this.autores.filter(option => option.nombre.toLowerCase().includes(filterValue));
  }

  displayFn(autor: AutorModel): string {
    console.log(autor);
    if(autor){
    console.log(autor);
    return autor.nombre;
    }
    else return '';
  }

  guardar( form: NgForm ) {

    //form.controls["nombre"].setValidators([Validators.required]);

    if ( form.invalid ) {
      this.formularioError = true;
      return;
    }

    Swal.fire({
      title: 'Espere',
      text: 'Guardando información',
      type: 'info',
      allowOutsideClick: false
    });
    Swal.showLoading();


    let peticion: Observable<any>;

    console.log(this.libro);
    
    console.log(this.autor.value);

  
    this.libro.autorId = this.autor.value.id;
    this.libro.categoriaId= this.categoriaControl.value;

    if ( this.libro.id ) {
      peticion = this.librosService.actualizarLibro( this.libro );
    } else {
      peticion = this.librosService.crearLibro( this.libro );
    }

    peticion.subscribe( resp => {

      Swal.fire({
        title: this.libro.nombre,
        text: 'Se actualizó correctamente',
        type: 'success'
      });

    });



  }

}
