import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map, startWith} from 'rxjs/operators';
import { LibrosService } from '../../services/libros.service';
import { LibroModel } from '../../models/libro.model';
import { FormControl } from '@angular/forms';
import { CategoriasService } from '../../services/categorias.service';
import { AutoresService } from '../../services/autores.service';
import { CategoriaModel } from 'src/app/models/categoria.model';
import { AutorModel } from 'src/app/models/autor.model';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-libros',
  templateUrl: './libros.component.html',
  styleUrls: ['./libros.component.css']
})
export class LibrosComponent implements OnInit {

  libros: LibroModel[] = [];
  cargando = false;

  categorias: CategoriaModel[] = [];
  autores: AutorModel[] = [];

  autor = new FormControl();
  categoria = new FormControl();
  nombre =  new FormControl();

  librosFiltrados: Observable<LibroModel[]>;

  constructor( private librosService: LibrosService, 
               private categoriasService: CategoriasService,
               private autoresService: AutoresService) { }

  ngOnInit() {

    this.categoriasService.getCategorias().subscribe( resp => {
      this.categorias = resp;
    });

    this.autoresService.getAutores().subscribe( resp => {
      this.autores = resp;
    });
    
    this.cargando = true;
  
    this.librosService.getLibros()
      .subscribe( resp => {
        this.libros = resp;     
        this.cargando = false;
        this.librosFiltrados = of(resp);
      });
   
  }


  onFilter(idFiltro:string, value:any) { 

    let libros = this.libros;

    if(value)
      libros = this.filtarLibros(idFiltro,value);

    this.librosFiltrados = of(libros);

  }

  filtarLibros(idFiltro:string, valor:string):LibroModel[]
  {
    const filterValue = valor.toLowerCase();
    let librosFiltrados:LibroModel[] = [];

    console.log(this.libros);

    if(idFiltro === 'autor'){
      librosFiltrados = librosFiltrados.concat(this.libros.filter(option => option.autorId.toLowerCase().includes(filterValue)));
      console.log(filterValue);
    }
    if(idFiltro === 'categoria'){
      librosFiltrados = librosFiltrados.concat(this.libros.filter(option => option.categoriaId.toLowerCase().includes(filterValue)));
    }
    if(idFiltro === 'nombre'){
      librosFiltrados = librosFiltrados.concat(this.libros.filter(option => option.nombre.toLowerCase().includes(filterValue)));
    }

    return librosFiltrados;

  }

  borrarLibro( libro: LibroModel, i: number ) {

    Swal.fire({
      title: '¿Está seguro?',
      text: `Está seguro que desea borrar a ${ libro.nombre }`,
      type: 'question',
      showConfirmButton: true,
      showCancelButton: true
    }).then( resp => {

      if ( resp.value ) {
        this.libros.splice(i, 1);
        this.librosService.borrarLibro( libro.id ).subscribe();
      }

    });



  }


}
