import { Component, OnInit } from '@angular/core';
import { AutoresService } from '../../services/autores.service';
import { AutorModel } from '../../models/autor.model';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-autores',
  templateUrl: './autores.component.html',
  styleUrls: ['./autores.component.css']
})
export class AutoresComponent implements OnInit {

  autores: AutorModel[] = [];
  cargando = false;


  constructor( private autoresService: AutoresService ) { }

  ngOnInit() {

    this.cargando = true;
    this.autoresService.getAutores()
      .subscribe( resp => {
        this.autores = resp;
        this.cargando = false;
      });

  }

  borrarAutor( autor: AutorModel, i: number ) {

    Swal.fire({
      title: '¿Está seguro?',
      text: `Está seguro que desea borrar a ${ autor.nombre }`,
      type: 'question',
      showConfirmButton: true,
      showCancelButton: true
    }).then( resp => {

      if ( resp.value ) {
        this.autores.splice(i, 1);
        this.autoresService.borrarAutor( autor.id ).subscribe();
      }

    });



  }


}
