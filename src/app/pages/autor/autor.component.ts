import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';

import { AutorModel } from '../../models/autor.model';
import { AutoresService } from '../../services/autores.service';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-autor',
  templateUrl: './autor.component.html',
  styleUrls: ['./autor.component.css']
})
export class AutorComponent implements OnInit {

  autor: AutorModel = new AutorModel('');
  formularioError:boolean = false;

  constructor( private autoresService: AutoresService,
               private route: ActivatedRoute ) { }

  ngOnInit() {

    const id = this.route.snapshot.paramMap.get('id');
    console.log(id);
    if ( id !== 'nuevo' ) {

      this.autoresService.getAutor( id )
        .subscribe( (resp: AutorModel) => {
          this.autor = resp;
          this.autor.id = id;
        });

    }

  }

  guardar( form: NgForm ) {

    if ( form.invalid ) {
      this.formularioError = true;
      console.log('Formulario no válido');
      return;
    }

    Swal.fire({
      title: 'Espere',
      text: 'Guardando información',
      type: 'info',
      allowOutsideClick: false
    });
    Swal.showLoading();


    let peticion: Observable<any>;

    if ( this.autor.id ) {
      peticion = this.autoresService.actualizarAutor( this.autor );
    } else {
      peticion = this.autoresService.crearAutor( this.autor );
    }

    peticion.subscribe( resp => {

      Swal.fire({
        title: this.autor.nombre,
        text: 'Se actualizó correctamente',
        type: 'success'
      });

    });



  }

}
