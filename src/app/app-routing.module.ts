import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LibrosComponent } from './pages/libros/libros.component';
import { LibroComponent } from './pages/libro/libro.component';
import { CategoriasComponent } from './pages/categorias/categorias.component';
import { CategoriaComponent } from './pages/categoria/categoria.component';
import { AutoresComponent } from './pages/autores/autores.component';
import { AutorComponent } from './pages/autor/autor.component';


const routes: Routes = [
  { path: 'libros', component: LibrosComponent },
  { path: 'libro/:id', component: LibroComponent },
  { path: 'categorias', component: CategoriasComponent },
  { path: 'categoria/:id', component: CategoriaComponent },
  { path: 'autores', component: AutoresComponent },
  { path: 'autor/:id', component: AutorComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'libros' }
];



@NgModule({
  imports: [
    RouterModule.forRoot( routes )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
