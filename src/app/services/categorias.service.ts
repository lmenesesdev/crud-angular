import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CategoriaModel } from '../models/categoria.model';
import { map, delay } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class CategoriasService {



  constructor(private http: HttpClient) {
 
  }

  private url = 'https://libraryadmin.azurewebsites.net';
  



  crearCategoria( categoria: CategoriaModel ) {

    return this.http.post(`${ this.url }/api/Categoria/Add`, categoria)
            .pipe(
              map( (resp: any) => {
                categoria.id = resp.name;
                return categoria;
              })
            );

  }

  actualizarCategoria( categoria: CategoriaModel ) {

    const categoriaTemp = {
      ...categoria
    };

    return this.http.put(`${ this.url }/api/Categoria/Update`, categoriaTemp);


  }

  borrarCategoria( id: string ) {

    return this.http.delete(`${ this.url }/api/Categoria/Delete?id=${ id }`);

  }


  getCategoria( id: string ) {

    return this.http.get(`${ this.url }/api/Categoria/GetById?id=${ id }`);

  }


  getCategorias() {
    return this.http.get(`${ this.url }/api/Categoria/GetAll`)
            .pipe(
              map( this.crearArreglo ),
              delay(0)
            );
  }

  private crearArreglo( categoriasObj: object ) {

    const categorias: CategoriaModel[] = [];

    Object.keys( categoriasObj ).forEach( key => {

      const categoria: CategoriaModel = categoriasObj[key];
      categorias.push( categoria );
    });


    return categorias;

  }


}
