import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AutorModel } from '../models/autor.model';
import { AuthServiceService } from './auth-service.service';
import { map, delay } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class AutoresService {

  private url = 'https://libraryadmin.azurewebsites.net';


  constructor( private http: HttpClient , private authService: AuthServiceService   ) { 

    console.log('llamando');
    this.authService.login('user','pass').subscribe();
  }


  crearAutor( autor: AutorModel ) {

    return this.http.post(`${ this.url }/api/Autor/Add`, autor)
            .pipe(
              map( (resp: any) => {
                autor.id = resp.id;
                return autor;
              })
            );

  }

  actualizarAutor( autor: AutorModel ) {

    const autorTemp = {
      ...autor
    };

    return this.http.put(`${ this.url }/api/Autor/Update`, autorTemp);

  }

  borrarAutor( id: string ) {

    return this.http.delete(`${ this.url }/api/Autor/Delete/?id=${ id }`);

  }


  getAutor( id: string ) {

    return this.http.get(`${ this.url }/api/Autor/GetById/?id=${ id }`);

  }


  getAutores() {
    return this.http.get(`${ this.url }/api/Autor/GetAll`)
            .pipe(
              map( this.crearArreglo ),
              delay(0)
            );
  }

  private crearArreglo( autoresObj: object ) {

    const autores: AutorModel[] = [];

    Object.keys( autoresObj ).forEach( key => {

      const autor: AutorModel = autoresObj[key];

      autores.push( autor );
    });


    return autores;

  }


}
