import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LibroModel } from '../models/libro.model';
import { map, delay } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class LibrosService {

  private url = 'https://libraryadmin.azurewebsites.net';

  constructor( private http: HttpClient ) { }


  crearLibro( libro: LibroModel ) {

    return this.http.post(`${ this.url }/api/Libro/Add`, libro)
            .pipe(
              map( (resp: any) => {
                libro.id = resp.id;
                return libro;
              })
            );

  }

  actualizarLibro( libro: LibroModel ) {

    const libroTemp = {
      ...libro
    };

    return this.http.put(`${ this.url }/api/Libro/Update`, libroTemp);


  }

  borrarLibro( id: string ) {

    return this.http.delete(`${ this.url }/api/Libro/Delete?id=${ id }`);

  }


  getLibro( id: string ) {

    return this.http.get(`${ this.url }/api/Libro/GetById?id=${ id }`);

  }


  getLibros() {
    return this.http.get(`${ this.url }/api/Libro/GetAll`)
            .pipe(
              map( this.crearArreglo ),
              delay(0)
            );
  }

  private crearArreglo( librosObj: object ) {

    const libros: LibroModel[] = [];

    Object.keys( librosObj ).forEach( key => {

      const libro: LibroModel = librosObj[key];

      libros.push( libro );
    });


    return libros;

  }


}
