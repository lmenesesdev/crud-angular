import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { map, delay, tap } from 'rxjs/operators';
import { UserLogin } from '../models/user.model';
import * as moment from "moment";

@Injectable({
  providedIn: 'root'
})
export class AuthServiceService {

  constructor(private http: HttpClient) {

  }

  login(userId:string, password:string ) {
    console.log(userId);
    console.log(password);
      return this.http.post('https://libraryadmin.azurewebsites.net/api/Login', new UserLogin('sssss','xxxxx'), { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) })
      .pipe(
        map( (resp: any) => {
          console.log('asdasdasddsa');
         this.setSession(resp)
        })
      );
  }
        
  private setSession(authResult) {
      const expiresAt = moment().add(authResult.expiresIn,'minute');

      localStorage.setItem('id_token', authResult.idToken);
      localStorage.setItem("expires_at", JSON.stringify(expiresAt.valueOf()) );
  }          

  logout() {
      localStorage.removeItem("id_token");
      localStorage.removeItem("expires_at");
  }

  public isLoggedIn() {
      return moment().isBefore(this.getExpiration());
  }

  isLoggedOut() {
      return !this.isLoggedIn();
  }

  getExpiration() {
      const expiration = localStorage.getItem("expires_at");
      const expiresAt = JSON.parse(expiration);
      return moment(expiresAt);
  }    

}
