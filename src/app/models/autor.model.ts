

export class AutorModel {

    id: string;
    nombre: string;
    apellidos: string;
    fechaNacimiento: string;

    constructor(nombre:string) {
        this.nombre = nombre;
    }

}

