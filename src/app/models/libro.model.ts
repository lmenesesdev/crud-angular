

export class LibroModel {

    id: string;
    nombre: string;
    autor: string;
    autorId: string;
    categoria: string;
    categoriaId: string;
    isbn:string;

    constructor() {

    }

}

